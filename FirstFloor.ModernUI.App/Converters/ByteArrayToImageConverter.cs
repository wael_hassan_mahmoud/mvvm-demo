﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace FirstFloor.ModernUI.App.Converters
{
    public class ByteArrayToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            byte[] byteBlob = value as byte[];
            if (byteBlob != null)
            {
                MemoryStream ms = new MemoryStream(byteBlob);
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.StreamSource = ms;
                image.EndInit();
                return image;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BitmapImage bitmapImage = value as BitmapImage;
            byte[] byteBlob = null;
            if (bitmapImage != null)
            {
                if (bitmapImage.StreamSource != null)
                {
                    byteBlob = ((MemoryStream)bitmapImage.StreamSource).ToArray();
                }
            }
            return byteBlob;
        }
    }
}
