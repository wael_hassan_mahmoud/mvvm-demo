﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FirstFloor.ModernUI.App
{
    public class FlowDirectionToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((string)parameter == "en")
            {
                return ((FlowDirection)value) == FlowDirection.LeftToRight ? true : false;
            }
            return ((FlowDirection)value) == FlowDirection.LeftToRight ? false : true;

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
