﻿#pragma checksum "..\..\Search.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "7F144C5C801943EA90D1D341EE235B23"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using FirstFloor.ModernUI.App;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Converters;
using FirstFloor.ModernUI.Windows.Navigation;
using GalaSoft.MvvmLight.Command;
using Northwind.ViewModel;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Animation;
using Telerik.Windows.Controls.Behaviors;
using Telerik.Windows.Controls.Carousel;
using Telerik.Windows.Controls.Data.PropertyGrid;
using Telerik.Windows.Controls.DragDrop;
using Telerik.Windows.Controls.GridView;
using Telerik.Windows.Controls.Legend;
using Telerik.Windows.Controls.MaskedInput;
using Telerik.Windows.Controls.Primitives;
using Telerik.Windows.Controls.RadialMenu;
using Telerik.Windows.Controls.TransitionEffects;
using Telerik.Windows.Controls.TreeListView;
using Telerik.Windows.Controls.TreeView;
using Telerik.Windows.Controls.Wizard;
using Telerik.Windows.Data;
using Telerik.Windows.DragDrop;
using Telerik.Windows.DragDrop.Behaviors;
using Telerik.Windows.Input.Touch;
using Telerik.Windows.Shapes;
using WPFLocalizeExtension.Engine;
using WPFLocalizeExtension.Extensions;
using WPFLocalizeExtension.Providers;
using WPFLocalizeExtension.TypeConverters;


namespace FirstFloor.ModernUI.App {
    
    
    /// <summary>
    /// Search
    /// </summary>
    public partial class Search : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 43 "..\..\Search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadMaskedNumericInput code;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\Search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadMaskedTextInput namee;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\Search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Telerik.Windows.Controls.RadMaskedTextInput namea;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ModernUIDemo;component/search.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Search.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 18 "..\..\Search.xaml"
            ((FirstFloor.ModernUI.App.Search)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.ModernDialog_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.code = ((Telerik.Windows.Controls.RadMaskedNumericInput)(target));
            
            #line 43 "..\..\Search.xaml"
            this.code.LostFocus += new System.Windows.RoutedEventHandler(this.code_LostFocus);
            
            #line default
            #line hidden
            
            #line 43 "..\..\Search.xaml"
            this.code.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.code_KeyDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.namee = ((Telerik.Windows.Controls.RadMaskedTextInput)(target));
            
            #line 46 "..\..\Search.xaml"
            this.namee.LostFocus += new System.Windows.RoutedEventHandler(this.namee_LostFocus);
            
            #line default
            #line hidden
            
            #line 46 "..\..\Search.xaml"
            this.namee.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.namee_KeyDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.namea = ((Telerik.Windows.Controls.RadMaskedTextInput)(target));
            
            #line 49 "..\..\Search.xaml"
            this.namea.LostFocus += new System.Windows.RoutedEventHandler(this.namea_LostFocus);
            
            #line default
            #line hidden
            
            #line 49 "..\..\Search.xaml"
            this.namea.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.namea_KeyDown);
            
            #line default
            #line hidden
            return;
            case 5:
            
            #line 59 "..\..\Search.xaml"
            ((Telerik.Windows.Controls.RadGridView)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.ModernDialog_KeyDown);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

