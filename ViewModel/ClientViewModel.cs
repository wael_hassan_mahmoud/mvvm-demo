﻿using Application;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    public class ClientViewModel : ViewModelBase
    {
        IUIDataProvider _uIDataProvider;
        public ClientViewModel(IUIDataProvider uIDataProvider)
        {
            _uIDataProvider = uIDataProvider;
        }

        public const string ClientsPropertyName = "Clients";
        private static ObservableCollection<Client> _clients;
        public ObservableCollection<Client> Clients
        {
            get
            {
                if (_clients == null)
                {
                    _clients = new ObservableCollection<Client>();
                }
                return _clients;
            }
            set
            {
                if (_clients == value)
                    return;

                _clients = value;

                RaisePropertyChanged(ClientsPropertyName);
            }
        }


        public const string ClientPropertyName = "Client";
        private static Client _client = null;
        public Client Client
        {
            get
            {
                return _client;
            }
            set
            {
                if (_client == value)
                    return;

                _client = value;

                RaisePropertyChanged(ClientPropertyName);
            }
        }


        private RelayCommand _loadCommand;
        public RelayCommand LoadCommand
        {
            get
            {
                return _loadCommand ??
                       (_loadCommand =
                        new RelayCommand(Load,canLoad));
            }
        }
        bool loading = false;
        public async void Load()
        {
            loading = true;
            LoadCommand.RaiseCanExecuteChanged();

            var lst = await _uIDataProvider.GetClientsAsync();

            Clients.Clear();
            foreach (var item in lst)
            {
                Clients.Add(item);
            }
            loading = false;
            LoadCommand.RaiseCanExecuteChanged();
        }
        public bool canLoad()
        {
            return !loading;
        }


        private RelayCommand _saveCommand;
        public RelayCommand SaveCommand
        {
            get
            {
                return _saveCommand ??
                       (_saveCommand =
                        new RelayCommand(Save,canSave));
            }
        }
        bool saving = false;
        public async void Save()
        {
            saving = true;
            SaveCommand.RaiseCanExecuteChanged();

            await _uIDataProvider.SaveClientsAsync(Clients.ToList());

            saving = false;
            SaveCommand.RaiseCanExecuteChanged();
        }
        public bool canSave()
        {
            return !saving;
        }


        private RelayCommand _delCommand;
        public RelayCommand DelCommand
        {
            get
            {
                return _delCommand ??
                       (_delCommand =
                        new RelayCommand(Del));
            }
        }
        public async void Del()
        {
            if (Client != null)
            {
                if (Client.ID != 0)
                {
                    await _uIDataProvider.DelClientAsync(Client.ID);
                }
                Clients.Remove(Client);
            }
        }


    }
}
