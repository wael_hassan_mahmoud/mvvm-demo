﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Client : ModelBase
    {
        public const string IDPropertyName = "ID";
        private int _iD;
        public int ID
        {
            get { return _iD; }
            set
            {
                if (_iD == value)
                    return;

                _iD = value;
                RaisePropertyChanged(IDPropertyName);
            }
        }


        public const string NomPropertyName = "Nom";
        private string _nom;
        public string Nom
        {
            get { return _nom; }
            set
            {
                if (_nom == value)
                    return;

                _nom = value;
                RaisePropertyChanged(NomPropertyName);
            }
        }


        public const string ProfessionPropertyName = "Profession";
        private string _profession;
        public string Profession
        {
            get { return _profession; }
            set
            {
                if (_profession == value)
                    return;

                _profession = value;
                RaisePropertyChanged(ProfessionPropertyName);
            }
        }
    }
}
