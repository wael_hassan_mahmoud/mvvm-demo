﻿using DataBaseEntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Service
{
    public class Service : IService
    {

        public Client[] GetClients()
        {
            using (codeprojectEntities Entities = new codeprojectEntities())
            {
                return Entities.Clients.Select(x => new Client() 
                {
                    ID = x.ID,
                    Nom = x.Nom,
                    Profession = x.Profession
                }).ToArray();
            }
        }

        public void SaveClients(ref Client[] clients)
        {
            using (codeprojectEntities Entities = new codeprojectEntities())
            {
                foreach (var item in clients)
                {
                    if (item.ID == 0)
                    {
                        Entities.Clients.Add(new DataBaseEntityModel.Client() 
                        {
                            Nom = item.Nom,
                            Profession = item.Profession
                        });
                    }
                    else
                    {
                        DataBaseEntityModel.Client tempClient = Entities.Clients.SingleOrDefault(x => x.ID == item.ID);
                        tempClient.Nom = item.Nom;
                        tempClient.Profession = item.Profession;
                    }
                }
                Entities.SaveChanges();
            }
        }

        public void DelClient(int id)
        {
            using (codeprojectEntities Entities = new codeprojectEntities())
            {
                Entities.Clients.Remove(Entities.Clients.SingleOrDefault(x => x.ID == id));
                Entities.SaveChanges();
            }
        }
    }
}
