﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        Client[] GetClients();

        [OperationContract]
        void SaveClients(ref Client[] clients);

        [OperationContract]
        void DelClient(int id);
    }

    [DataContract]
    public class Client
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Nom { get; set; }
        [DataMember]
        public string Profession { get; set; }
    }

}
