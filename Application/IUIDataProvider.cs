﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public interface IUIDataProvider
    {
        Task<List<Client>> GetClientsAsync();

        Task SaveClientsAsync(List<Client> clients);

        Task DelClientAsync(int id);

    }
}
