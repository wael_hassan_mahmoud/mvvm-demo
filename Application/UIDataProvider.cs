﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class UIDataProvider : IUIDataProvider
    {
        public async Task<List<Model.Client>> GetClientsAsync()
        {
            using (var serviceClient = new ServiceReference1.ServiceClient())
            {
                return await Task.Run(() =>
                {
                    List<Model.Client> lst = new List<Model.Client>();

                    foreach (var item in serviceClient.GetClients())
                    {
                        lst.Add(new Model.Client() 
                        {
                            ID = item.ID,
                            Nom = item.Nom,
                            Profession = item.Profession
                        });
                    }

                    return lst;
                });
            }
        }

        public async Task SaveClientsAsync(List<Model.Client> clients)
        {
            using (var serviceClient = new ServiceReference1.ServiceClient())
            {
                await Task.Run(() =>
                {
                    ServiceReference1.Client[] srvLst = new ServiceReference1.Client[clients.Count];

                    for (int i = 0; i < clients.Count; i++)
                    {
                        srvLst[i] = new ServiceReference1.Client();
                        srvLst[i].ID = clients[i].ID;
                        srvLst[i].Nom = clients[i].Nom;
                        srvLst[i].Profession = clients[i].Profession;
                    }

                    serviceClient.SaveClients(ref srvLst);

                    for (int i = 0; i < srvLst.Length; i++)
                    {
                        clients[i].ID = srvLst[i].ID;
                    }
                });
            }
        }

        public async Task DelClientAsync(int id)
        {
            using (var serviceClient = new ServiceReference1.ServiceClient())
            {
                await Task.Run(() =>
                {
                    serviceClient.DelClient(id);
                });
            }
        }
    }
}
